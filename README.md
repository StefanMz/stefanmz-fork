# Commons Booking – 0.5 beta
![Logo](./commons-booking/assets/cb-logo.png)

Wordpress plugin for managing and booking of common goods. 

Florian Egermann / wielebenwir e.V. 

## Unique features:

* Items, locations and timeframes: Set the locations and timeframes (when the item is bookable at that location) for each of your items. 
* Auto-accept bookings: A registered user can book items without the need for administration. 
* Simple booking process with a beautiful calendar: Click a day to book an item. 


## Further information
* Project Website: http://www.wielebenwir.de/projekte/gemeingutsoftware
* Manual: http://dein-lastenrad.de/index.php?title=Introduction
* Bug-Tracker: https://bitbucket.org/wielebenwir/commons-booking/issues?status=new&status=open 
* Forum (Ask questions here): http://forum.dein-lastenrad.de/index.php?p=/categories/buchungs-software


## Credits

* Built with [The WordPress Plugin Boilerplate Powered ](https://github.com/sudar/wp-plugin-in-github/wiki) 
* Uses [CMB2](https://github.com/WebDevStudios/Custom-Metaboxes-and-Fields-for-WordPress)
* Uses [CPT_Core](https://github.com/WebDevStudios/CPT_Core)
* Uses [Taxonomy_Core ]( https://github.com/WebDevStudios/Taxonomy_Core )
* German translation by Sven Baier & Stefan Meretz
   

##License

> This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License, version 2, as
published by the Free Software Foundation.

> This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.